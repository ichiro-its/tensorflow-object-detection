# ObjectDetection

## Install Tensorflow

- Install **Tensorflow**.
  ``` sh
  ~$ sudo pip install tensorflow
  ```

- Install **Tensorflow-GPU**.
  ``` sh
  ~$ sudo pip install tensorflow-gpu
  ```

## Install Tensorflow Models API

- Clone this repository `https://github.com/tensorflow/models`.

## Install Dependencies
- Install **Dependencies**.
  ``` sh
  ~$ sudo apt-get install protobuf-compiler python-pil python-lxml python-tk
  ~$ pip install --user Cython
  ~$ pip install --user contextlib2
  ~$ pip install --user jupyter
  ~$ pip install --user matplotlib
  ```
- Install **COCO API**.
  ``` sh
  ~$ git clone https://github.com/cocodataset/cocoapi.git
  ~$ cd cocoapi/PythonAPI
  ~$ make
  ~$ cp -r pycocotools <path_to_models>/research
  ```

## Protobuf Compilation
- Compile **Protobuf** libraries from `models/research`.
  ``` sh
  ~$ protoc object_detection/protos/*.proto --python_out=.
  ```

## Add Libraries to PYTHONPATH
- Add libraries to **PYTHONPATH** from `models/research`.
  ``` sh
  ~$ export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
  ```

## Test The Installation
- Test the **API**.
  ``` sh
  ~$ python object_detection/builders/model_builder_test.py
  ```

## Label Pictures

- Convert video to images.
  ``` sh
  ~$ python Scripts/python/video_to_mat.py --video_dir=Data/Video/live.avi --output_dir=Data/Image
  ```

## Label Pictures

- For image labelling use the **Modified-labelImg** from `https://gitlab.com/ICHIRO-ITS/modified-labelimg`.

## Generate Training Data

- Convert `xml` data into `objectdetection_labels.csv`
  ```
  ~$ python Scripts/python/xml_to_csv.py
  ```

- Generate `train.record`
  ```
  ~$ python Scripts/python/generate_tfrecord.py --csv_input=Data/objectdetection_labels.csv --image_dir=Data/Image --output_path=Data/train.record
  ```

### Label map

- Create `object-detection.pbtxt` in the `Training` to label the map which contains these
  ```
  item {
    id: 1
    name: 'ball'
  }
  ```

## Training 

- Run the training
  ```
  ~$ python train.py --logtostderr --train_dir=Training/ --pipeline_config_path=Training/ssd_mobilenet_v2_coco.config
  ```

- To view the progress of the training job, use **TensorBoard** from `models/research/object_detection/legacy` directory
  ```
  ~$ tensorboard --logdir=Training
  ```

## Export Inference Graph

- Generate the **Frozen InferenceGraph**. Change `XXXX` in `model.ckpt-XXXX` with the highest-numbered `.ckpt` inside `Training` folder:
  ```
  ~$ python export_inference_graph.py --input_type image_tensor --pipeline_config_path Training/ssd_mobilenet_v2_coco.config --trained_checkpoint_prefix Training/model.ckpt-XXXX --output_directory Training/inference_graph
  ```

## Run The Test

- Test the classifier using
  ```
  ~$ python Scripts/python/objectdetection.py
  ```
