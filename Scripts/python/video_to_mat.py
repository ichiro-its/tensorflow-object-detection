import cv2 
import tensorflow as tf
import os

flags = tf.app.flags
flags.DEFINE_string('video_dir', '', 'Path to video')
flags.DEFINE_string('output_dir', '', 'Path to output')
FLAGS = flags.FLAGS
  
def FrameCapture(path):       
    vidObj = cv2.VideoCapture(path)   
    count = 0
    success = 1
  
    while success: 
  
        success, image = vidObj.read() 
        cv2.imwrite(os.path.join(FLAGS.output_dir)+"frame%d.jpg" % count, image) 
        count += 1
  
if __name__ == '__main__': 
    FrameCapture(os.path.join(FLAGS.video_dir))