import numpy as np
import tensorflow as tf
import cv2 as cv
import time

# Read the graph.
with tf.gfile.FastGFile('Training/inference_graph/frozen_inference_graph.pb', 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

with tf.Session() as sess:
    # Restore session
    sess.graph.as_default()
    tf.import_graph_def(graph_def, name='')

    # Read and preprocess an image.
    cap = cv.VideoCapture(0)

    starttime = time.time()
    frame = 0

    while True :
        
        frame = frame + 1
        ret, img = cap.read()
        rows = img.shape[0]
        cols = img.shape[1]
        inp = img
        inp = inp[:, :, [2, 1, 0]]  # BGR2RGB

        # Run the model
        out = sess.run([sess.graph.get_tensor_by_name('num_detections:0'),
                        sess.graph.get_tensor_by_name('detection_scores:0'),
                        sess.graph.get_tensor_by_name('detection_boxes:0'),
                        sess.graph.get_tensor_by_name('detection_classes:0')],
                        feed_dict={'image_tensor:0': inp.reshape(1, inp.shape[0], inp.shape[1], 3)})

        # Visualize detected bounding boxes.
        num_detections = int(out[0][0])
        for i in range(num_detections):
            classId = int(out[3][0][i])
            score = float(out[1][0][i])
            bbox = [float(v) for v in out[2][0][i]]
            if score > 0.9 and classId == 1:
                print(score)
                # labels = ["Ball", "Goal", "L_junction", "T_junction", "X_junction", "Penalty_point"]
                x = bbox[1] * cols
                y = bbox[0] * rows
                right = bbox[3] * cols
                bottom = bbox[2] * rows
                cv.rectangle(img, (int(x), int(y)), (int(right), int(bottom)), (125, 255, 51), thickness=2)
                cv.putText(img, "Ball", (int(x), int(y)), cv.FONT_HERSHEY_SIMPLEX,  
                   1, (255, 0, 0), 2, cv.LINE_AA)

        cv.putText(img, str((int)(frame / (time.time() - starttime))), (5, 30), cv.FONT_HERSHEY_DUPLEX, 1.0, (255, 0, 0), lineType=cv.LINE_AA)
        
        if time.time() > starttime + 5:
            starttime = time.time()
            frame = 0
        
        cv.imshow('TensorFlow MobileNet-SSD', img)
        
        if cv.waitKey(1) == 27:
            cap.release()
            cv.destroyallwindows()
            break
